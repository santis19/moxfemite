Moxfemite
=========

Another osx-like theme for XFCE and Gtk+

Based on MosEmite, Greybird and Xosemite themes.

How to Install?
---------------

First install dependencies. In Debian/Ubuntu based distros:

    $ sudo apt-get install gtk2-murrine-engines

Then copy the moxemite folder in ~/.themes for single user (create it if it does not exists):

    $ mkdir ~/.themes

    $ cp -r moxfemite ~/.themes/

Or copy it in /usr/share/themes for system wide usage:

    $ sudo cp -r moxfemite /usr/share/themes/


Fix Qt Applications
-------------------

If your Qt Applications don't wear this theme, then install the following package:

    $ sudo apt-get install qt4-qtconfig

Then run:

    $ qtconfig

And choose GTK in the GUI Style options.
